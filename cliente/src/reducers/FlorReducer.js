import { FLOR_ABELHAS, FLOR_MESES, FLOR_SAVE, FLOR_LIST} from './../actions/action_types';
const INITIAL_STATE = [{}];

export default function Flor(state = INITIAL_STATE, action) {
    switch (action.type) {
        case FLOR_ABELHAS:
            return { ...state, data: action.payload.data };
        case FLOR_MESES:
            return { ...state, meses: action.payload.data };
        case FLOR_SAVE:
            return { ...state, save: action.payload.data };
        case FLOR_LIST:
            return { ...state, list: action.payload.data };
        default:
            return state;
    }
}


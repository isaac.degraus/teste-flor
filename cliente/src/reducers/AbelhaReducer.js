import { ABELHA_SAVE } from './../actions/action_types';
const INITIAL_STATE = [{}];

export default function Abelha(state = INITIAL_STATE, action) {
    switch (action.type) {
        case ABELHA_SAVE:
            return { save: action.payload.data };
        default:
            return state;
    }
}


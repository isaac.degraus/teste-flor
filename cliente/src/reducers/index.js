import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import Flor from './FlorReducer';
import Abelha from './AbelhaReducer';
export const Reducers = combineReducers({
   form: reduxFormReducer,
   Flor:Flor,
   Abelha:Abelha
});
import React from 'react';
import { BrowserRouter, Route, Switch} from "react-router-dom";
import Home from './../container/Home';
import Flor from './../componets/flor';
import Abelha from '../componets/abelha';


const Routes = () => (
    <BrowserRouter>
    <Switch>
    
      <Route exact path="/" component={Home} />
      <Route exact path="/flor" component={Flor} />
      <Route exact path="/abelha" component={Abelha} />
    
    </Switch>
  </BrowserRouter>
)
export default Routes;
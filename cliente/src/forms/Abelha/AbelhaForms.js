import React, { Component } from 'react'
import { Field, reduxForm, Form, reset } from 'redux-form'
import { connect } from 'react-redux'
import { renderField, validate, warn } from './Validations'


class AbelhaForms extends Component {



   componentDidUpdate() {
        if (this.props.savestatus) {
            this.props.reseta();
        }
    }

    render() {
        const { onSubmit, onChange,savestatus } = this.props
        const msg = (savestatus) => {
            
            if (savestatus) {
                return (<div class="alert alert-success" role="alert">
                    Salvo Com Sucesso
                </div>);
            }
        }
        return (
            <>
                <div className={['container-fluid background-form abelha']}> <h2 className={['title-cad']}>Cadastre abelhas</h2> </div>
                <div className={['container']}>
                {msg(savestatus)}
                    <Form onSubmit={onSubmit}>
                        <label>Nome</label>
                        <Field
                            name="nome"
                            label="Qual o nome da abelha"
                            type="text"
                            value=''
                            onChange={onChange}
                            component={renderField}
                            className="form-control"></Field>
                        <label>Espécie</label>
                        <Field name="especie"
                            label="Qual a espécie ou nome científico"
                            type="text"
                            value=''
                            component={renderField}
                            onChange={onChange}
                            className="form-control">

                        </Field>

                        <div className="form-group">
                            <button type="submit" className="btn btn-primary">Cadastrar</button>
                        </div>

                    </Form>

                </div>
            </>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        savestatus: store.Abelha.save,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reseta: () => dispatch(reset('AbelhaForms'))
    }
}


export default connect(
    mapStateToProps, mapDispatchToProps
)(reduxForm({
    form: 'AbelhaForms',
    enableReinitialize: true,// faz pega os pre dados 
    validate,
    warn
})(AbelhaForms));
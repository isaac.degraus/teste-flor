import React, { Component } from 'react'
import { Field, reduxForm, Form, reset } from 'redux-form'
import { connect } from 'react-redux'
import { renderField, validate, warn, file_upload } from './Validations'
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css'; // Import Sun Editor's CSS File
import Select from 'react-select'
import './../../styles.css';
import { florAbelhasThunck, florMesesThunck } from './../../actions/FlorAction';


class FlorForms extends Component {

    state = {
        selectedOption: null,
        messelecionado: []
    };
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.myRefEnun = React.createRef();


    }


    UNSAFE_componentWillMount() {
        this.props.florAbelhas();
        this.props.florMeses();
    }


    componentDidUpdate() {
        if (this.props.savestatus) {
            this.props.reseta();
            if (this.myRefEnun.current !== null) {
                this.myRefEnun.current.editor.setContents('')
            }
            if (this.state.selectedOption !== null) {
                // eslint-disable-next-line
                this.state.selectedOption = null
            }
            if (this.state.messelecionado !== []) {
                // eslint-disable-next-line
                this.state.messelecionado = []
            }
        }

    }


    handleChange = selectedOption => {
        this.setState(
            { selectedOption },
        );
        this.props.inputChangeAbelhas(selectedOption.map((v) => v.value));
    };

    marca = (v) => {
        if (!this.state.messelecionado.includes(v)) {
            this.state.messelecionado.push(v);
        } else {
            const index = this.state.messelecionado.indexOf(v);
            if (index > -1) {
                this.state.messelecionado.splice(index, 1);
            }
        }
        this.props.inputChangeMeses(this.state.messelecionado);
        this.setState({ teste: '1' });
    }

    render() {
        const { onSubmit, onChange, Texto, abelhasList, mesesList, savestatus } = this.props
        const { selectedOption } = this.state;
        let abelhas = [];
        console.log(savestatus);
        abelhas = abelhasList?.map((value) => {
            return { value: value.id, label: value.nome }
        })

        let meses = mesesList?.map((value) => {
            return <button key={value.id + 'mes'} type="button" className={[' btn btn-light meses']} style={{ 'background': (this.state.messelecionado.includes(value.id)) ? '#78769D' : '#fff' }} onClick={() => { this.marca(value.id) }}>{value.meses}</button>
        });
        const msg = (savestatus) => {

            if (savestatus) {
                return (<div class="alert alert-success" role="alert">
                    Salvo Com Sucesso
                </div>);
            }
        }
        return (
            <>

                <div className={['container-fluid background-form']}> <h2 className={['title-cad']}>Cadastre flores</h2> </div>
                <div className={['container']}>
                    {msg(savestatus)}


                    <Form onSubmit={onSubmit}>
                        <div className={['row']}>
                            <div className={['col-md-12']}>
                                <h4>Cadastre as flores de acordo com o mês em que ela floresce</h4>
                            </div>
                        </div>
                        <div className={['col-md-12']}>
                            <div className={['row']}>
                                <div className={['col-md-8']}>
                                    <label>Nome</label>
                                    <Field
                                        name="nome"
                                        label="Qual o nome da flor"
                                        type="text"
                                        value=''
                                        onChange={onChange}
                                        component={renderField}
                                        className="form-control"></Field>
                                    <label>Espécie</label>
                                    <Field name="especie"
                                        placeholder="Espécie"
                                        label="Qual a espécie ou nome científico"
                                        type="text"
                                        value=''
                                        component={renderField}
                                        onChange={onChange}
                                        className="form-control">

                                    </Field>
                                </div>
                                <div className={['col-md-4 text-center']}>
                                    <img className="  mx-auto d-block img-responsive" alt='' src={require('./../../assets/foto.png')} onClick={() => this.myRef.current.click()} style={{ width: '150px', height: '150px' }} />
                                    <Field refa={this.myRef} name="img" component={file_upload} type="file" style={{ 'display': 'none' }} />
                                    <label>ESCOLHA UMA IMAGEM</label>
                                </div>


                            </div>
                            <div className={['row']}>
                                <div className={['col-md-12']}>
                                    <SunEditor ref={this.myRefEnun} name="descricao" placeholder="Escreva uma breve descrição sobre a flor" setContents="" onChange={(e) => Texto(e)} setOptions={{
                                        height: 200,
                                        buttonList: [['font', 'align', 'fontSize', 'paragraphStyle', 'removeFormat'], 'fontSize']
                                    }} />
                                </div>
                            </div>
                            <div className={['row mt-3']}>
                                <div className={['col-md-12']}>
                                    <h5>Quais  os meses a flor irá florescer</h5>
                                    {meses}
                                </div>
                            </div>
                            <div className={['row mt-3']}>
                                <div className={['col-md-12']}>
                                    <h5>Selecione as abelhas que polinizam essas flores</h5>
                                    <Select
                                        name='abelhas'
                                        value={selectedOption}
                                        isMulti
                                        onChange={this.handleChange}
                                        options={abelhas}
                                    />
                                </div>
                            </div>
                            <div className={['row mt-3']}>
                                <div className={['col-md-12']}>
                                    <div className="form-group float-right">
                                        <button type="reset" className="btn btn-outline-primary butao-rest">Cancelar</button>
                                        <button type="submit" className="btn btn-primary butao-cad">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </Form>

                </div>
            </>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        abelhasList: store.Flor.data,
        mesesList: store.Flor.meses,
        savestatus: store.Flor.save,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        florAbelhas: () => dispatch(florAbelhasThunck()),
        florMeses: () => dispatch(florMesesThunck()),
        reseta: () => dispatch(reset('FlorForms'))
    }
}


export default connect(
    mapStateToProps, mapDispatchToProps
)(reduxForm({
    form: 'FlorForms',
    enableReinitialize: true,// faz pega os pre dados 
    validate,
    warn
})(FlorForms));
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { abelhaSaveThunck } from '../actions/AbelhaAction';
import AbelhaForms from '../forms/Abelha/AbelhaForms';
import Footer from './footer';

class Abelha extends Component {
   
    submit = (e) => {
        e.preventDefault();
        this.props.save(this.state)
      
    }
    inputchange = (target) => {
        this.setState({Abelha:target});
    }
 
    render() {

        return (
            <>
                <AbelhaForms  onChange={this.inputchange} onSubmit={this.submit}></AbelhaForms>
                <Footer></Footer>
            </>
        );
    }
}


const mapStateToProps = (store) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        save:(data) => dispatch(abelhaSaveThunck(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Abelha);


import React, { Component } from 'react';
import { connect } from 'react-redux';
import FlorForms from '../forms/Flor/FlorForms';
import { florSaveThunck } from './../actions/FlorAction';
import Footer from './footer';

class Flor extends Component {

    submit = (e) => {
        e.preventDefault();
        this.props.save(this.state)

    }
    inputchange = (target) => {
        this.setState({ Flor: target });
    }
    inputChangeAbelhas = (target) => {
        this.setState({ Abelhas: target });
    }
    inputChangeMeses = (target) => {
        this.setState({ Meses: target });
    }
    texto = (target) => {
        /* eslint eqeqeq: 0 */
        return this.setState({ desc: target });
    }
    render() {



        return (
            <>
                <FlorForms Texto={this.texto} onChange={this.inputchange} inputChangeMeses={this.inputChangeMeses} inputChangeAbelhas={this.inputChangeAbelhas} onSubmit={this.submit}></FlorForms>
               <Footer></Footer>
            </>
        );
    }
}


const mapStateToProps = (store) => {
    return {
        save: store.Flor.save,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        save: (data) => dispatch(florSaveThunck(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Flor);


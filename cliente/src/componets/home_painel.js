import React, { Component } from 'react';
import { connect } from 'react-redux'
import MenuHome from './../componets/menu_home';
import Select from 'react-select'
import { florAbelhasThunck, florMesesThunck, florListThunck, florFilterThunck } from './../actions/FlorAction';
import { urlImage } from './../api/Config';
import './../styles.css';
import ReactPaginate from 'react-paginate';
import Modal from 'react-modal';
class HomePainel extends Component {
    state = {
        selectedOption: [],
        messelecionado: [],
        currentPage: 0,
        modal: false,
        florAtiva: null
    };

    UNSAFE_componentWillMount() {
        this.props.florAbelhas();
        this.props.florMeses();
        this.props.flores();
    }


    handleChange = selectedOption => {
        this.setState(
            { selectedOption },
        );
        this.props.filtrar({ meses: this.state.messelecionado, abelhas: selectedOption?.map((v) => v.value) });
    };

    marca = (v) => {
        if (!this.state.messelecionado.includes(v)) {
            this.state.messelecionado.push(v);
        } else {
            const index = this.state.messelecionado.indexOf(v);
            if (index > -1) {
                this.state.messelecionado.splice(index, 1);
            }
        }
        this.props.filtrar({ meses: this.state.messelecionado, abelhas: this.state.selectedOption?.map((v) => v.value) });
        this.setState({ teste: '1' });
    }

    handlePageClick = ({ selected: selectedPage }) => {
        this.setState({ currentPage: selectedPage });


    };
    openModal = (v) => {
        this.setState({ florAtiva: { ...v }, modal: true });
    }

    closeModal = () => {
        this.setState({ modal: false });
    }

    render() {
        const PER_PAGE = 12;
        const { abelhasList, mesesList, florList } = this.props
        const { selectedOption, currentPage, modal, florAtiva } = this.state;
        let abelhas = [];
        let flores = [];
        const offset = currentPage * PER_PAGE;
        const pageCount = Math.ceil(florList?.length / PER_PAGE);
        const customStyles = {
            content: {
                top: '50%',
                left: '50%',
                right: 'auto',
                bottom: 'auto',
                marginRight: '-50%',
                transform: 'translate(-50%, -50%)',
                width:'420px',
                border:'0px'
            }
        };

        abelhas = abelhasList?.map((value) => {
            return { value: value.id, label: value.nome }
        })

        let meses = mesesList?.map((value) => {
            return <button key={value.id + 'mes'} type="button" style={{ 'background': (this.state.messelecionado.includes(value.id)) ? '#78769D' : '#fff' }} onClick={() => { this.marca(value.id) }} className={[' btn btn-light meses']}>{value.meses}</button>
        });

        flores = florList?.slice(offset, offset + PER_PAGE).map((value) => {
            return <div onClick={() => { this.openModal(value) }} className={['col-md-3']}><img src={`${urlImage}/${value.img}`} alt="..." className={[' mx-auto d-block img-flor']} /><p className={['text-center text-flor']}>{value.nome}</p></div>
        });


        return (
            <>
                <MenuHome></MenuHome>
                <div className={['container']}>
                    <h5>Selecione as abelhas</h5>
                    <Select
                        name='abelhas'
                        value={selectedOption}
                        isMulti
                        onChange={this.handleChange}
                        options={abelhas}
                    />
                    <div className={['container mt-3']}>
                        <div className={['row']}>
                            <h5>Escolha os meses</h5>
                        </div>
                        <div className={['row']}>
                            {meses}
                        </div>
                    </div>


                    <div className={['container mt-3']}>
                        <div className={['row']}>
                            {flores}
                        </div>
                        <ReactPaginate
                            previousLabel={"<"}
                            nextLabel={">"}
                            pageCount={pageCount}
                            onPageChange={this.handlePageClick}
                            containerClassName={"pagination"}
                            previousLinkClassName={"pagination__link"}
                            nextLinkClassName={"pagination__link"}
                            disabledClassName={"pagination__link--disabled"}
                            activeClassName={"pagination__link--active"}
                        />
                    </div>
                </div>

                <Modal
                    isOpen={modal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >

                    <div className="card">
                        <img src={`${urlImage}/${florAtiva?.img}`} className="img-box" alt="..." />
                        <div className="card-body">
                            <span className="home">{florAtiva?.nome}</span>
                            <p className="card-text" dangerouslySetInnerHTML={{__html:florAtiva?.descricao}}></p>
                            <span className="home">Abelhas</span>
                            <p className="card-text">{florAtiva?.abelhas?.map((v)=> v.nome+' ')}</p>
                        </div>
                        <button className={'close'} onClick={this.closeModal}>x</button>
                    </div>
                </Modal>

            </>
        )
    }
}

const mapStateToProps = (store) => {
    return {
        abelhasList: store.Flor.data,
        mesesList: store.Flor.meses,
        florList: store.Flor.list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        florAbelhas: () => dispatch(florAbelhasThunck()),
        florMeses: () => dispatch(florMesesThunck()),
        flores: () => dispatch(florListThunck()),
        filtrar: (dados) => dispatch(florFilterThunck(dados)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePainel);
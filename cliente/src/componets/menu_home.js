import React from 'react';
import { Link } from 'react-router-dom';
import './../styles.css';
function MenuHome() {

    return (
        <>


            <nav className="navbar navbar-expand-lg navbar-light ">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <div className={['container d-none d-sm-none d-lg-block .d-xl-block .d-md-block']}>
                        <div className="row mt-3">
                            <div className="col-md-7">
                                <div className="row">
                                    <h1 className="home">Calendário de flores</h1>
                                </div>
                                <div className="row">
                                    <div className="col-md-7">
                                        <p>Neste calendário encontram-se diversas flores. Podem ser agupada pelos meses que florescem e o pelo tipo de abelha que poliniza a flor.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3" style={{ display: 'grid' }}>
                                <Link class={'btn btn-outline-primary butao-rest home'} to={'/flor'}>Cadastrar flor</Link>
                                <Link class={'btn btn-outline-primary butao-rest home mt-3'} to={'/abelha'}>Cadastrar abelha</Link>
                            </div>
                        </div>

                    </div>

                    <ul class="navbar-nav d-lg-none">
                    <h1 className="home">Calendário de flores</h1>
                        <li class="nav-item">
                            <Link class={'nav-link'} to={'/flor'}>Cadastrar flor</Link>
                        </li>
                        <li class="nav-item">
                            <Link class={'nav-link'} to={'/abelha'}>Cadastrar abelha</Link>
                        </li>
                    </ul>

                </div>
            </nav>


        </>
    )

}
export default MenuHome
import  axios  from 'axios';
import{url,token} from'./Config'

const api = axios.create({
    baseURL: url,
    timeout: 50000,
    headers: {'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`}
});

export default api;

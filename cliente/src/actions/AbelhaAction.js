import { ABELHA_SAVE } from './action_types';
import api from './../api/Api'




export function abelhaSave(data = false) {
    return {
        type: ABELHA_SAVE,
        payload: { data: data }
    }
}




export function abelhaSaveThunck(dados) {
    return async (dispatch) => {

        try {

            await api.post('abelha', dados.Abelha );
    
            dispatch(abelhaSave(true));
        } catch (error) {
            dispatch(abelhaSave());
        }

    }
}




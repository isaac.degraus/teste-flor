export const LOGIN_SERVER = 'LOGIN_SERVER';
export const USER_SERVER = 'USER_SERVER';
export const USER_SAVE = 'USER_SAVE';
export const INITIAL_GET = 'INITIAL_GET';
export const INITIAL_GETALL = 'INITIAL_GETALL';
export const USER_PUT = 'USER_PUT';
///////////////////////
export const FLOR_MESES = 'FLOR_MESES';
export const FLOR_ABELHAS = 'FLOR_ABELHAS';
export const FLOR_SAVE = 'FLOR_SAVE';
export const FLOR_LIST = 'FLOR_LIST';
export const FLOR_FILTRAR = 'FLOR_FILTRAR';
//////////////////
export const ABELHA_SAVE = 'ABELHA_SAVE';

import { FLOR_ABELHAS, FLOR_MESES, FLOR_SAVE,FLOR_LIST } from './action_types';
import api from './../api/Api'


export function florAbelhas(data = []) {
    return {
        type: FLOR_ABELHAS,
        payload: { data: data }
    }
}

export function florMeses(data = []) {
    return {
        type: FLOR_MESES,
        payload: { data: data }
    }
}


export function florList(data = []) {
    return {
        type: FLOR_LIST,
        payload: { data: data }
    }
}

export function florSave(data = false) {
    return {
        type: FLOR_SAVE,
        payload: { data: data }
    }
}

export function florAbelhasThunck() {
    return async (dispatch) => {

        try {
            const { data } = await api.get('abelha/');
            dispatch(florAbelhas(data));
        } catch (error) {
            dispatch(florAbelhas([{}]));
        }

    }
}
export function florListThunck() {
    return async (dispatch) => {

        try {
            const { data } = await api.get('flor');
            dispatch(florList(data));
        } catch (error) {
            dispatch(florList([{}]));
        }

    }
}


export function florFilterThunck(dados) {
    return async (dispatch) => {

        try {
            const { data } = await api.post('flor/filter',dados);
            dispatch(florList(data));
        } catch (error) {
            dispatch(florList([{}]));
        }

    }
}



export function florMesesThunck() {
    return async (dispatch) => {

        try {
            const { data } = await api.get('flor/mes');
            dispatch(florMeses(data));
        } catch (error) {
            dispatch(florMeses([{}]));
        }

    }
}
export function florSaveThunck(dados) {
    return async (dispatch) => {

        try {

            let data = new FormData();
            data.append('img', dados.Flor.img[0]);
            data.append('nome', dados.Flor.nome);
            data.append('especie', dados.Flor.especie);
            data.append('descricao', dados.desc);

            const flor = await api.post('/flor', data, {
                headers: {
                    "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
                }
            }); 
            data = {'flor_id':flor.data.id,'abelhas':dados.Abelhas}

             await api.post('flor/abelha', data );
           
             data = {'flor_id':flor.data.id,'meses':dados.Meses}

             await api.post('flor/mes', data );
            dispatch(florSave(true));
        } catch (error) {
            dispatch(florSave());
        }

    }
}




CREATE DATABASE  IF NOT EXISTS `abelhas_flores` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `abelhas_flores`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: abelhas_flores
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abelha_flor`
--

DROP TABLE IF EXISTS `abelha_flor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abelha_flor` (
  `flor_id` int(11) NOT NULL,
  `abelha_id` int(11) NOT NULL,
  PRIMARY KEY (`flor_id`,`abelha_id`),
  KEY `fk_flores_has_abelhas_abelhas1_idx` (`abelha_id`),
  KEY `fk_flores_has_abelhas_flores_idx` (`flor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abelha_flor`
--

LOCK TABLES `abelha_flor` WRITE;
/*!40000 ALTER TABLE `abelha_flor` DISABLE KEYS */;
/*!40000 ALTER TABLE `abelha_flor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abelhas`
--

DROP TABLE IF EXISTS `abelhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abelhas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(145) NOT NULL,
  `especie` varchar(145) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abelhas`
--

LOCK TABLES `abelhas` WRITE;
/*!40000 ALTER TABLE `abelhas` DISABLE KEYS */;
INSERT INTO `abelhas` VALUES (1,'Uruçu','Melipona scutellaris','2020-11-27 00:00:00','2020-11-27 00:00:00'),(28,'Uruçu-Amarela','Uruçu-Amarela','2020-11-27 00:00:00','2020-11-27 00:00:00'),(29,'Guarupu','Melipona bicolor','2020-11-27 00:00:00','2020-11-27 00:00:00'),(30,'Iraí','Nannotrigona testaceicornes','2020-11-27 00:00:00','2020-11-27 00:00:00');
/*!40000 ALTER TABLE `abelhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flor_mes`
--

DROP TABLE IF EXISTS `flor_mes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flor_mes` (
  `flor_id` int(11) NOT NULL,
  `mes_id` int(11) NOT NULL,
  PRIMARY KEY (`flor_id`,`mes_id`),
  KEY `fk_flores_has_meses_meses1_idx` (`mes_id`),
  KEY `fk_flores_has_meses_flores1_idx` (`flor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flor_mes`
--

LOCK TABLES `flor_mes` WRITE;
/*!40000 ALTER TABLE `flor_mes` DISABLE KEYS */;
/*!40000 ALTER TABLE `flor_mes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flores`
--

DROP TABLE IF EXISTS `flores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(145) NOT NULL,
  `especie` varchar(145) NOT NULL,
  `descricao` text NOT NULL,
  `img` varchar(145) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flores`
--

LOCK TABLES `flores` WRITE;
/*!40000 ALTER TABLE `flores` DISABLE KEYS */;
/*!40000 ALTER TABLE `flores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meses`
--

DROP TABLE IF EXISTS `meses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meses` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meses`
--

LOCK TABLES `meses` WRITE;
/*!40000 ALTER TABLE `meses` DISABLE KEYS */;
INSERT INTO `meses` VALUES (1,'jan'),(2,'fev'),(3,'mar'),(4,'abr'),(5,'mai'),(6,'jun'),(7,'jul'),(8,'ago'),(9,'set'),(10,'out'),(11,'nov'),(12,'dez');
/*!40000 ALTER TABLE `meses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'abelhas_flores'
--

--
-- Dumping routines for database 'abelhas_flores'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-26 14:10:08

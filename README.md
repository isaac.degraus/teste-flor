Teste pratico Cadastro de Flores.

 Tecnologias usa das:
  - LARAVEL BACK-END
  - REACTJS FRONT-END
  - DOCKER

# Rodando com docker!

  - Vá até a pasta cliente execute npm install  
  - Na raiz do projeto execulte  docker-compose up -d
  - após concluir a instalação execute os seguintes comandos:
  -      docker exec app-react composer install
  -     docker exec app-react php artisan config:cache
  -     docker exec app-react php artisan storage:link
  -     docker exec app-react php artisan config:cache --env=testing
  - Para roda os testes execute: docker exec app-react php ./vendor/bin/phpunit

Informações da aplicação:
  - A aplicação estará rodando em  http://localhost:3001/
  - usuário e senha do banco de dados   usuario:root senha:1234

# Rodando sem docker!

  - Vá até a pasta cliente execute npm install  
  - Vá até a pasta api abra o arquivo .env.semdocker e copie tudo e subistitua no arquivo .env  
  - Vá até a pasta api execute composer install  
  - após concluir a instalação execute os seguintes comandos:
  -     php artisan config:cache
  -      php artisan storage:link
  -     php artisan config:cache --env=testing
  - Vá na pasta api execute: php artisan serve
  - Vá na pasta cliente execute: npm start
  - Para roda os testes execute: php ./vendor/bin/phpunit

Informações da aplicação:
  - A aplicação estará rodando em  http://localhost:3000/
  - usuário e senha do banco de dados   usuario:root senha:

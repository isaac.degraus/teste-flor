<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AbelhaUnitTest extends TestCase
{
  
    public function testAbelhaCreatedTest()
    {
        
           $this->json('POST', 'api/abelha',['nome'=>'phpunitteste','especie'=>'phpunitteste'] ,['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure(['nome','especie','updated_at','created_at','id']);
       
    }

    public function testAbelhaListTest()
    {
        
           $response = $this->json('GET', 'api/abelha',['Accept' => 'application/json'])->assertStatus(200);
           $response->assertSuccessful();
           
    }
}

<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class FlorUnitTest extends TestCase
{
    public function testFlorCreatedTest()
    {
        
           $response = $this->json('POST', 'api/flor',['nome'=>'phpunitteste','especie'=>'phpunitteste','descricao'=>'phpunitteste','img'=>UploadedFile::fake()->image('file.png', 600, 600)] ,['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure(['nome','especie','updated_at','created_at','id','img']);

             $this->json('POST', 'api/flor/mes',['flor_id'=>$response['id'],'meses'=>[1,2,3]] ,['Accept' => 'application/json'])
            ->assertStatus(200);
           

            $this->json('POST', 'api/flor/abelha',['flor_id'=>$response['id'],'abelhas'=>[1]] ,['Accept' => 'application/json'])
            ->assertStatus(200);
    }
    public function testFlorListTest()
    {
        
           $response = $this->json('GET', 'api/flor',['Accept' => 'application/json'])->assertStatus(200);
           $this->assertNotEmpty($response->original);
           $response->assertSuccessful();
            
    }

    public function testFlorFilterTest()
    {
        
           $response = $this->json('POST', 'api/flor/filter',['meses'=>[1],'abelhas'=>[1]] ,['Accept' => 'application/json'])->assertStatus(200);
           $response->assertSuccessful();
           $this->assertNotEmpty($response->original);
            
    }
}

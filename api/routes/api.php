<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\RegisterController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('abelha', 'Abelha\AbelhaController@create');
Route::get('abelha', 'Abelha\AbelhaController@list');
Route::post('flor', 'Flor\FlorController@create');
Route::get('flor', 'Flor\FlorController@index');
Route::post('flor/filter', 'Flor\FlorController@filter');
Route::get('flor/mes', 'Flor\FlorController@list_meses');
Route::post('flor/abelha', 'Flor\FlorController@create_flor_abelha');
Route::post('flor/mes', 'Flor\FlorController@create_flor_meses');
<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FlorAbelha extends Pivot
{
    protected $table='abelha_flor';
}

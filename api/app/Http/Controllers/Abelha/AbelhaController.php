<?php

namespace App\Http\Controllers\Abelha;

use App\Abelha;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbelhaController extends Controller
{

    protected $abelha = null;
    public function __construct(Abelha $abelha)
    {
        $this->abelha = $abelha;
    }

    protected function validar(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:145'],
            'especie' => ['required', 'string', 'max:145'],
        ]);
    }

    protected function create(Request $request)
    {
        $data = $request->all();
        $this->validar($data);
        return $this->abelha::create($data);
    }

    protected function list()
    {
        
       
        return $this->abelha->all()->toArray();
    }
}

<?php

namespace App\Http\Controllers\Flor;

use App\Flor;
use App\Http\Controllers\Controller;
use App\Mes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class FlorController extends Controller
{
    protected $flor = null;
    public function __construct(Flor $flor)
    {
        $this->flor = $flor;
    }

    protected function index()
    {
        return $this->flor->with('abelhas')->get()->toArray();
    }

    protected function validar(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:145'],
            'espcie' => ['required', 'string', 'max:145'],
        ]);
    }

    protected function create(Request $request)
    {
        $data = $request->all();


        if ($request->file('img')) {
            $ext = explode('.', $request->file('img')->getClientOriginalName());
            $name = (new \DateTime())->getTimestamp();
            $img = $request->file('img')->storeAs('img', $name . '.' . $ext[1], 'public');
        }
        $data['img'] = $img;
        return $this->flor::create($data);
    }

    protected function list_meses()
    {
        return  Mes::all();
    }


    protected function create_flor_abelha(Request $request)
    {
        $data  =  $request->all();
        $flor = $this->flor::find($data['flor_id']);
        $flor->abelhas()->attach($data['abelhas']);
    }

    protected function create_flor_meses(Request $request)
    {
        $data  =  $request->all();
        $flor = $this->flor::find($data['flor_id']);
        $flor->meses()->attach($data['meses']);
    }

    protected function filter(Request $request)
    {
        $data  =  $request->all();
      
        $flor = $this->flor->whereHas('meses', function ($q) use ($data) {
            if (!empty($data['meses'])) {
                $q->whereIn('mes_id', $data['meses']);
            }
        })->whereHas('abelhas', function ($q)  use ($data) {
            if (!empty($data['abelhas'])) {
                $q->whereIn('abelha_id', $data['abelhas']);
            }
        })->with(['meses', 'abelhas'])->get()->toArray();
        return $flor;
    }
}

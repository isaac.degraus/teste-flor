<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flor extends Model
{
    protected $table ='flores';
    protected $fillable = ['nome', 'especie', 'descricao', 'img'] ;

    public  function meses(){
        return $this->belongsToMany(Mes::class)->using(FlorMes::class);
    }

    public  function abelhas(){
        return $this->belongsToMany(Abelha::class)->using(FlorAbelha::class);
    }
}

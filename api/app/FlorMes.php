<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FlorMes extends Pivot
{
    protected $table='flor_mes';
}
